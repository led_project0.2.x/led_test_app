package com.example.leddisplay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import org.apache.commons.lang3.SerializationUtils;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;
import yuku.ambilwarna.AmbilWarnaDialog;

class SendDataClass implements Serializable{
    byte x_pos;
    byte y_pos;
    byte red_col;
    byte green_col;
    byte blue_col;
}

public class MainActivity extends AppCompatActivity {

    public BluetoothSPP bt;
    int dotColorValue;

    public static final byte STX = (byte)0x02;
    public static final byte ETX = (byte)0x03;

    public static final byte SET_DRAW = (byte)0x11;
    public static final byte SET_TEXT = (byte)0x21;
    public static final byte SET_SCR_CLEAR = (byte)0x31;
    public static final byte SAVE_TEXT = (byte)0xA1;
    public static final byte SET_COLOR_TEXT = (byte)0x51;

    public static final byte CMD_ACT_LEFT_RIGHT = (byte)0x1;
    public static final byte CMD_ACT_RIGHT_LEFT = (byte)0x2;
    public static final byte CMD_ACT_TOP_DOWN = (byte)0x3;
    public static final byte CMD_ACT_DOWN_TOP = (byte)0x4;
    public static final byte CMD_ACT_BLINK = (byte)0x5;





    final int crc16tab[]= {
            0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
            0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
            0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
            0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
            0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
            0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
            0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
            0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
            0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
            0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
            0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
            0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
            0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
            0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
            0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
            0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
            0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
            0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
            0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
            0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
            0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
            0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
            0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
            0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
            0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
            0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
            0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
            0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
            0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
            0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
            0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
            0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
    };

    int tb_row = 32;
    int tb_col = 64;

    boolean[][] matrix_array = new boolean[tb_row][tb_col];

    int dotColorSel = 0xffff0000;
    int checkCount=0;

    TableRow[] tbRow = new TableRow[tb_row];
    TextView[][] tvCols = new TextView[tb_row][tb_col];

    private View.OnTouchListener tvListener = new View.OnTouchListener() {

        Drawable color_draw;

        @SuppressLint("ResourceType")
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            for (int i=0; i<tb_row; i++){
                for(int j=0; j<tb_col; j++) {
                    if(tvCols[i][j].getId() ==  v.getId())
                    {
                        if(matrix_array[i][j] == true) {
                            color_draw = getResources().getDrawable(R.drawable.table_inside_gray);
                            tvCols[i][j].setBackground(color_draw);
                            matrix_array[i][j] = false;
                            tvCols[i][j].setBackgroundColor(Color.parseColor(getResources().getString(R.color.black)));
                            ColorDrawable color = (ColorDrawable)(tvCols[i][j].getBackground());
                            send_single_dot((byte)j, (byte)i, color.getColor());
                            tvCols[i][j].setBackground(color_draw);

                        }
                        else {
                            matrix_array[i][j] = true;
                            dotColorValue = dotColorSel;
                            tvCols[i][j].setBackgroundColor(dotColorValue);
                            send_single_dot((byte)j, (byte)i, dotColorValue); // convert 255 to 32 level
                        }
                    }
                }
            }
            return false;
        }
    };

    public void openColorPicker() {
        AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, dotColorValue, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
            }
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                dotColorSel = color;
            }
        });
        colorPicker.show();
    }


    int tbRowIdBase = 100;
    int tvColsIdBase = 1000;
    public void table_view_make()
    {
        TableLayout tb_layout = (TableLayout)findViewById(R.id.tb_layout);

        for (int i=0; i<tb_row; i++){

            tbRow[i] = new TableRow(this);
            tbRow[i].setId(tbRowIdBase + i);
            tb_layout.addView(tbRow[i]);

            for (int j=0; j<tb_col; j++)
            {
                tvCols[i][j] = new TextView(this);
                tvCols[i][j].setId(tvColsIdBase + (i*tb_col)+j);
                tvCols[i][j].setBackgroundColor(Color.GRAY);
                tvCols[i][j].setBackground(this.getResources().getDrawable(R.drawable.table_inside_gray));
                tvCols[i][j].setOnTouchListener(tvListener);
                tbRow[i].addView(tvCols[i][j]);
            }
        }
    }

    public static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        table_view_make();

        Button btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SubActivity.class);
                startActivity(intent);
            }
        });

        RadioGroup rg_color = (RadioGroup)findViewById(R.id.rg_color);

        rg_color.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.rb_red) {
                    dotColorSel = 0xffff0000;
                }
                else if(checkedId == R.id.rb_green)
                {
                    dotColorSel = 0xff00ff00;
                }
                else if(checkedId == R.id.rb_blue){
                    dotColorSel = 0xff0000ff;
                }
                else if(checkedId == R.id.rb_custom){
                    openColorPicker();
                }


            }
        });

        bluetoothInit();
    }

    public void bluetoothInit(){
        bt = new BluetoothSPP(this); //Initializing
        if (!bt.isBluetoothAvailable()) { //블루투스 사용 불가
            Toast.makeText(getApplicationContext(), "Bluetooth is not available", Toast.LENGTH_SHORT).show();
            finish();
        }

        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() { //데이터 수신
            public void onDataReceived(byte[] data, String message) {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() { //연결됐을 때
            public void onDeviceConnected(String name, String address) {
                Toast.makeText(getApplicationContext()
                        , "Connected to " + name + "\n" + address
                        , Toast.LENGTH_SHORT).show();
            }

            public void onDeviceDisconnected() { //연결해제
                Toast.makeText(getApplicationContext()
                        , "Connection lost", Toast.LENGTH_SHORT).show();
            }

            public void onDeviceConnectionFailed() { //연결실패
                Toast.makeText(getApplicationContext()
                        , "Unable to connect", Toast.LENGTH_SHORT).show();
            }
        });

        Button btnConnect = findViewById(R.id.btnConnect); //연결시도
        btnConnect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (bt.getServiceState() == BluetoothState.STATE_CONNECTED) {
                    bt.disconnect();
                } else {
                    Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                    startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
                };
            }
        });
    }
    public void onDestroy() {
        super.onDestroy();
        bt.stopService(); //블루투스 중지
    }

    public void onStart() {
        super.onStart();
        if (!bt.isBluetoothEnabled()) { //
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if (!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_OTHER); //DEVICE_ANDROID는 안드로이드 기기 끼리
                setup();
            }
        }
    }

    public void screen_clear()
    {
        Button btnScrClear = findViewById(R.id.btnScrClear); //데이터 전송
        btnScrClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                byte[] data = new byte[1];
                data[0] = SET_SCR_CLEAR;
                transmit_data(bt, data);
                Drawable color_draw;

                for (int i=0; i<tb_row; i++){
                    for(int j=0; j<tb_col; j++) {

                            color_draw = getResources().getDrawable(R.drawable.table_inside_gray);
                            tvCols[i][j].setBackground(color_draw);
                            matrix_array[i][j] = false;
                    }
                }
            }
        });
    }

    public void send_single_dot(byte xPos, byte yPos, int colorValue)
    {
        byte red_col;
        byte green_col;
        byte blue_col;
        byte[] sendData = new byte[9];
        int inx = 0;
        byte page_num = 1;
        short dotLen = 1;

        red_col = (byte)(((colorValue & 0x00ff0000) >> 16) / 8);
        green_col = (byte)(((colorValue & 0x0000ff00) >> 8) / 8);
        blue_col = (byte)(((colorValue & 0x000000ff) >> 0) / 8);

        sendData[inx++] = SET_DRAW;
        sendData[inx++] = page_num;
        sendData[inx++] = (byte)(dotLen >> 8);
        sendData[inx++] = (byte)dotLen;

        sendData[inx++] = xPos;
        sendData[inx++] = yPos;
        sendData[inx++] = red_col;
        sendData[inx++] = green_col;
        sendData[inx++] = blue_col;
        transmit_data(bt, sendData);
    }

    public void send_all_dot()
    {
        final byte[][] dot_pos = new byte[tb_row][tb_col];

        checkCount = 0;
        for (int i=0; i<tb_row; i++){
            for(int j=0; j<tb_col; j++) {
                dot_pos[i][j] = (byte)(matrix_array[i][j]?1:0);
                if(dot_pos[i][j] == 1)
                    ++checkCount;
            }
        }

        int cnt = 0;
        int colorValue;
        SendDataClass[] send = new SendDataClass[checkCount];

        for (int i=0; i<tb_row; i++){
            for(int j=0; j<tb_col; j++) {
                if(dot_pos[i][j] == 1) {
                    ColorDrawable color = (ColorDrawable)(tvCols[i][j].getBackground());
                    colorValue = color.getColor();
                    send[cnt] = new SendDataClass();
                    send[cnt].x_pos = (byte)j;
                    send[cnt].y_pos = (byte)i;
                    send[cnt].red_col = (byte)((colorValue & 0x00ff0000) >> 16);
                    send[cnt].green_col = (byte)((colorValue & 0x0000ff00) >> 8);
                    send[cnt].blue_col = (byte)((colorValue & 0x000000ff) >> 0);
                    ++cnt;
                }
            }
        }

        byte[] sendData = new byte[(checkCount * 5)+4];
        int inx = 0;
        byte page_num = 1;
        short dotLen = (short)checkCount;

        sendData[inx++] = SET_DRAW;
        sendData[inx++] = page_num;
        sendData[inx++] = (byte)(dotLen >> 8);
        sendData[inx++] = (byte)dotLen;

        for (int i=0; i<checkCount; i++)
        {
            sendData[inx++] = send[i].x_pos;
            sendData[inx++] = send[i].y_pos;
            sendData[inx++] = send[i].red_col;
            sendData[inx++] = send[i].green_col;
            sendData[inx++] = send[i].blue_col;
        }
        transmit_data(bt, sendData);
    }


    public void setup() {
        Button btnSend = findViewById(R.id.btnSend); //데이터 전송

        btnSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                send_all_dot();
            }
        });


        screen_clear();
    }



    public short crc16_ccitt(byte[] data)
    {
        int counter;
        short crc = 0;
        for(counter = 0; counter < data.length; counter++)
            crc = (short)((crc<<8) ^ crc16tab[((crc>>8) ^ data[counter])&0x00FF]);
        return crc;
    }

    public void transmit_data(BluetoothSPP ba, byte[] data)
    {
        Log.d("send", "transmit data!!");

        short crc;
        short dataLen = (short)data.length;
        byte[] packet = new byte[5 + dataLen + 3];

        packet[0] = (STX);
        packet[1] = (byte)(dataLen);
        packet[2] = ((byte)(dataLen >> 8));
        packet[3] = ((byte)(dataLen));
        packet[4] = ((byte)(dataLen >> 8));

        System.arraycopy(data, 0, packet, 5, dataLen);

        crc = crc16_ccitt(data);

        packet[dataLen + 5] = (byte)(crc >> 8);
        packet[dataLen + 6] = (byte)(crc);
        packet[dataLen + 7] = ETX;

        ba.send(packet, false);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if (resultCode == Activity.RESULT_OK)
                bt.connect(data);
        } else if (requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_OTHER);
                setup();
            } else {
                Toast.makeText(getApplicationContext()
                        , "Bluetooth was not enabled."
                        , Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}
