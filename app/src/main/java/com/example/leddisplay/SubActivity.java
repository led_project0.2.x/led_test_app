package com.example.leddisplay;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;
import yuku.ambilwarna.AmbilWarnaDialog;


public class SubActivity extends AppCompatActivity {
    int colorFontSel=0xff0000;
    String colorScrSel="Black";


    int mDefaultColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        Button btnPrev = (Button)findViewById(R.id.btnPrev);
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        font_color_select();
        screen_color_select();
        action_time();
        set_font_size();
    }

    public void openColorPicker() {
        AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, mDefaultColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
            }
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                mDefaultColor = color;
                colorFontSel = color & 0x00ffffff;
            }
        });
        colorPicker.show();
    }

    public void font_color_select()
    {
        RadioGroup rg_color = (RadioGroup)findViewById(R.id.rg_color2);
        rg_color.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.rb_red2) {
                    colorFontSel = 0xff0000;
                }
                else if(checkedId == R.id.rb_green2)
                {
                    colorFontSel = 0x00ff00;
                }else if(checkedId == R.id.rb_blue2){
                    colorFontSel = 0x0000ff;
                }
                else if(checkedId == R.id.rb_custom2){
                    openColorPicker();
                }

            }
        });
    }

    public void screen_color_select()
    {
        RadioGroup rg_scr_color = (RadioGroup)findViewById(R.id.rg_scr_color);
        rg_scr_color.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                byte red_col;
                byte green_col;
                byte blue_col;
                byte[] sendData = new byte[3];

                if(checkedId == R.id.rb_scr_red)
                {
                    red_col = (byte)0xff;
                    green_col = (byte)0x00;
                    blue_col = (byte)0x00;
                    colorScrSel = "Red";
                }
                else if(checkedId == R.id.rb_scr_green)
                {
                    red_col = (byte)0x00;
                    green_col = (byte)0xff;
                    blue_col = (byte)0x00;
                    colorScrSel = "Green";
                }
                else if(checkedId == R.id.rb_scr_blue)
                {
                    red_col = (byte)0x00;
                    green_col = (byte)0x00;
                    blue_col = (byte)0xff;
                    colorScrSel = "Blue";
                }
                else
                {
                    red_col = (byte)0x00;
                    green_col = (byte)0x00;
                    blue_col = (byte)0x00;
                    colorScrSel = "Black";
                }
            }
        });
    }

    public void action_time()
    {
        SeekBar sb  = (SeekBar) findViewById(R.id.seekBarActionTime);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                TextView tv = (TextView)findViewById(R.id.tv_action_time);
                tv.setText(String.valueOf(progress * 10));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void set_font_size()
    {
        SeekBar sb  = (SeekBar) findViewById(R.id.seekBarFontSize);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                TextView tv = (TextView)findViewById(R.id.tvFontSize);
                tv.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void save_text_btn_setup() {

        Button btnSend = findViewById(R.id.btnTextSave); //데이터 전송
        final EditText et_text = (EditText)findViewById(R.id.et_text);


        btnSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String textBoxData = et_text.getText().toString();
                mSendText(textBoxData,  ((MainActivity) MainActivity.context).SAVE_TEXT);
            }
        });
    }

    public void send_btn_setup() {
        Button btnSend = findViewById(R.id.btnSend2); //데이터 전송
        final EditText et_text = (EditText)findViewById(R.id.et_text);

        btnSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String textBoxData = et_text.getText().toString();
                mSendText(textBoxData, ((MainActivity) MainActivity.context).SET_TEXT);
            }
        });
    }

    public void mSendText(String textBoxData, byte cmd) {
        short dataLen = 0;
        int inx = 0;
        byte[] sendData;
        List dataList = new ArrayList();
        byte[] textData = textBoxData.getBytes();
        RadioButton rbAction0 = (RadioButton) findViewById(R.id.rb_action_0);
        RadioButton rbAction1 = (RadioButton) findViewById(R.id.rb_action_1);
        RadioButton rbAction2 = (RadioButton) findViewById(R.id.rb_action_2);
        RadioButton rbAction3 = (RadioButton) findViewById(R.id.rb_action_3);
        RadioButton rbAction4 = (RadioButton) findViewById(R.id.rb_action_4);
        RadioButton rbAction5 = (RadioButton) findViewById(R.id.rb_action_5);
        TextView tv_action_time = (TextView) findViewById(R.id.tv_action_time);
        TextView tvFontSize = (TextView) findViewById(R.id.tvFontSize);

        byte textGroupCount = 3;
        if (rbAction0.isChecked() == true)
            dataLen = (short) ((textData.length + 1 + 3) * textGroupCount + 1 + 3 + 1 + 1); // page num(1) + Screen Color(3) + Text Size(1) + Font Color(3), Font Size(1) + Text Group Count(1)
        else
            dataLen = (short) ((textData.length + 1 + 3) * textGroupCount + 1 + 3 + 1 + 1 + 3); // page num(1) + Screen Color(3) + Text Size(1) + Font Color(3), Font Size(1) + Text Group Count(1) + Action(3 : action, time)

        sendData = new byte[dataLen + 1];

        byte page_num = 1;
        sendData[inx++] = cmd;
        sendData[inx++] = 0;    //  옵션 값
        sendData[inx++] = page_num;
        sendData[inx++] = (byte) (textData.length >> 8);
        sendData[inx++] = (byte) (textData.length);
        System.arraycopy(textData, 0, sendData, inx, textData.length);
        inx = inx + textData.length;
        byte fontSize = (byte) Integer.parseInt(tvFontSize.getText().toString());
        sendData[inx++] = (byte) fontSize;

        // Font Color
        sendData[inx++] = (byte) ((colorFontSel & 0x00ff0000) >> 16);
        sendData[inx++] = (byte) ((colorFontSel & 0x0000ff00) >> 8) ;
        sendData[inx++] = (byte) ((colorFontSel & 0x000000ff) >> 0) ;

        // BG Color
        if (colorScrSel == "Red") {
            sendData[inx++] = (byte) 0xff; // RED
            sendData[inx++] = (byte) 0x00; // GREEN
            sendData[inx++] = (byte) 0x00; // BLUE
        } else if (colorScrSel == "Green") {
            sendData[inx++] = (byte) 0x00; // RED
            sendData[inx++] = (byte) 0xff; // GREEN
            sendData[inx++] = (byte) 0x00; // BLUE
        } else if (colorScrSel == "Blue") {
            sendData[inx++] = (byte) 0x00; // RED
            sendData[inx++] = (byte) 0x00; // GREEN
            sendData[inx++] = (byte) 0xff; // BLUE
        } else if (colorScrSel == "Black") {
            sendData[inx++] = (byte) 0x00; // RED
            sendData[inx++] = (byte) 0x00; // GREEN
            sendData[inx++] = (byte) 0x00; // BLUE
        }

        //action
        if (rbAction1.isChecked() == true)
            sendData[inx++] = (byte) ((MainActivity) MainActivity.context).CMD_ACT_LEFT_RIGHT;
        else if (rbAction2.isChecked() == true)
            sendData[inx++] = (byte) ((MainActivity) MainActivity.context).CMD_ACT_RIGHT_LEFT;
        else if (rbAction3.isChecked() == true)
            sendData[inx++] = (byte) ((MainActivity) MainActivity.context).CMD_ACT_TOP_DOWN;
        else if (rbAction4.isChecked() == true)
            sendData[inx++] = (byte) ((MainActivity) MainActivity.context).CMD_ACT_DOWN_TOP;
        else if (rbAction5.isChecked() == true)
            sendData[inx++] = (byte) ((MainActivity) MainActivity.context).CMD_ACT_BLINK;

        //action time
        int action_time = Integer.parseInt(tv_action_time.getText().toString());
        sendData[inx++] = (byte) ((action_time & 0x0000ff00) >> 8);
        sendData[inx++] = (byte) (action_time & 0x000000ff);

        sendData[inx++] = 0;   //효과
        sendData[inx++] = 0;   //효과 시간1
        sendData[inx++] = 0;   //효과 시간2

        sendData[inx++] = 0;   // 테두리 색상 r
        sendData[inx++] = 0;   // 테두리 색상 g
        sendData[inx++] = 0;   // 테두리 색상 b

        sendData[inx++] = 0;   // 효과 색상 변경 속도
        sendData[inx++] = 0;   // 눈꽃 시간 1
        sendData[inx++] = 0;   // 눈꽃 시간 2


        ((MainActivity) MainActivity.context).transmit_data(((MainActivity) MainActivity.context).bt, sendData);
    }

    public void setup() {
        send_btn_setup();
        save_text_btn_setup();

        Button btnSetColorText = findViewById(R.id.btnSetColorText);


        btnSetColorText.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                //SET_COLOR_TEXT
                set_color_text();
            }
        });

    }

    public void set_color_text()
    {
        int idx = 0;

        String text = "안녕";
        byte[] text_bytes = text.getBytes();

        String text2 = "하세요";
        byte[] text_bytes2 = text2.getBytes();

        byte[] data = new byte[9 + text_bytes.length + 4 + text_bytes2.length + 15];

        data[idx++] = (byte)MainActivity.SET_COLOR_TEXT;
        data[idx++] = 0; // option cmd
        data[idx++] = 0; // page number 0~4

        data[idx++] = 0; // screen color
        data[idx++] = 0; // screen color
        data[idx++] = 0; // screen color

        data[idx++] = 1; // font size 1~4
        data[idx++] = 2; // text group count

        //-----------------------------text group 1---------------------------------
        data[idx++] = (byte)text_bytes.length; // text size
        for(int i = 0; i < text_bytes.length; i++) // string n bytes
        {
            data[idx++] = (byte)text_bytes[i];
        }
        data[idx++] = 31;     // font color r
        data[idx++] = 0;      // font color g
        data[idx++] = 0;      // font color b
        //--------------------------------------------------------------------------

        //-----------------------------text group 2---------------------------------
        data[idx++] = (byte)text_bytes2.length;      // text size
        for(int i = 0; i < text_bytes2.length; i++)  // string (n bytes)
        {
            data[idx++] = (byte)text_bytes2[i];
        }
        data[idx++] = 0;  // font color r
        data[idx++] = 0;  // font color g
        data[idx++] = 31; // font color b
        //--------------------------------------------------------------------------

        data[idx++] = 0; // action cmd 0~5
        data[idx++] = 0; // action time 상위 8비트
        data[idx++] = 0; // action time 하위 8비트

        data[idx++] = 0; // effect cmd
        data[idx++] = 0; // effect time 상위 8비트
        data[idx++] = 0; // effect time 하위 8비트

        data[idx++] = 0; // 테두리 색깔 r
        data[idx++] = 0; // 테두리 색깔 g
        data[idx++] = 0; // 테두리 색깔 b

        data[idx++] = 2; // effect color change value 2~12

        data[idx++] = 0; // effect rain time 상위 8비트
        data[idx++] = 0; // effect rain time 하위 8비트

        ((MainActivity) MainActivity.context).transmit_data(((MainActivity) MainActivity.context).bt, data);
    }

    public void onStart() {
        super.onStart();
        setup();
    }
}
